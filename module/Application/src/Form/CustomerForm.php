<?php

namespace Application\Form;

use Laminas\Form\Form;

class CustomerForm extends Form
{
    public function __construct($name = null)
    {
        // We will ignore the name provided to the constructor
        parent::__construct();

        $this->add([
            'name' => 'id',
            'type' => 'hidden',
        ]);
        $this->add([
            'name' => 'firstname',
            'type' => 'text',
            'options' => [
                'label' => 'Firstname',
            ],
        ]);
        $this->add([
            'name' => 'lastname',
            'type' => 'text',
            'options' => [
                'label' => 'Lastname',
            ],
        ]);
        $this->add([
            'name' => 'startdate',
            'type' => 'text',
            'options' => [
                'label' => 'Start Date',
            ],
        ]);
        $this->add([
            'name' => 'enddate',
            'type' => 'text',
            'options' => [
                'label' => 'End Date',
            ],
        ]);
        $this->add([
            'name' => 'policynumber',
            'type' => 'text',
            'options' => [
                'label' => 'Policy Number',
            ],
        ]);
        $this->add([
            'name' => 'premium',
            'type' => 'text',
            'options' => [
                'label' => 'Premium',
            ],
        ]);
        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Go',
                'id'    => 'submitbutton',
            ],
        ]);
    }
}

?>