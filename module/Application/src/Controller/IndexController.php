<?php

declare(strict_types=1);

namespace Application\Controller;

use Application\Model\Customertable;
use Application\Model\Customer;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use Application\Form\CustomerForm;

class IndexController extends AbstractActionController
{
    private $table;

    public function __construct(Customertable $table){
        $this->table = $table;
    }
    
    public function indexAction()
    {
        return new ViewModel(
            [
                'customers' => $this->table->fetchAll()
            ]
        );
    }

    public function addAction(){

        $form = new CustomerForm();
        $form->get('submit')->setValue('Add');

        $request = $this->getRequest();
        // print_r(!$request->isPost());exit;

        if (!$request->isPost()) {
            return ['form' => $form];
        }

        $customer = new Customer();
        $form->setInputFilter($customer->getInputFilter());
        $form->setData($request->getPost());

        if (! $form->isValid()) {
            return ['form' => $form];
        }

        $customer->exchangeArray($form->getData());
        // print_r($customer);exit;
        $this->table->saveCustomer($customer);
        return $this->redirect()->toRoute('home');
    }

    public function editAction(){
        
        $id = (int) $this->params()->fromRoute('id', 0);

        if (0 === $id) {
            return $this->redirect()->toRoute('add', ['action' => 'add']);
        }

        try {
            $customer = $this->table->getCustomer($id);
        } catch (\Exception $e) {
            return $this->redirect()->toRoute('home', ['action' => 'index']);
        }

        $form = new CustomerForm();
        $form->bind($customer);
        $form->get('submit')->setAttribute('value', 'Edit');

        $request = $this->getRequest();
        $viewData = ['id' => $id, 'form' => $form];

        if (! $request->isPost()) {
            return $viewData;
        }

        $form->setInputFilter($customer->getInputFilter());
        $form->setData($request->getPost());

        if (! $form->isValid()) {
            return $viewData;
        }

        try {
            $this->table->saveCustomer($customer);
        } catch (\Exception $e) {
        }

        // Redirect to album list
        return $this->redirect()->toRoute('home', ['action' => 'index']);
    }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('home');
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->getPost('del', 'No');

            if ($del == 'Yes') {
                $id = (int) $request->getPost('id');
                $this->table->deleteCustomer($id);
            }

            // Redirect to list of albums
            return $this->redirect()->toRoute('home');
        }

        return [
            'id'    => $id,
            'customer' => $this->table->getCustomer($id),
        ];
    }

    public function viewAction(){
        $id = (int) $this->params()->fromRoute('id', 0);
        if (!$id) {
            return $this->redirect()->toRoute('home');
        }

        return [
            'id'    => $id,
            'customer' => $this->table->getCustomer($id),
        ];
    }
}
