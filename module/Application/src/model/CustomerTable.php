<?php
namespace Application\Model;

use RuntimeException;
use Laminas\Db\TableGateway\TableGatewayInterface;

class CustomerTable
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        return $this->tableGateway->select();
    }

    public function getCustomer($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (! $row) {
            throw new RuntimeException(sprintf(
                'Could not find row with identifier %d',
                $id
            ));
        }

        return $row;
    }

    public function saveCustomer(Customer $customer)
    {
        $data = [
            'firstname' => $customer->firstname,
            'lastname'  => $customer->lastname,
            'startdate'  => $customer->startdate,
            'enddate'  => $customer->enddate,
            'policynumber'  => $customer->policynumber,
            'premium'  => $customer->premium,
            'added_date'  => date("Y-m-d h:i:s"),
            'updated_date'  => date("Y-m-d h:i:s")
        ];

        $id = (int) $customer->id;
        if ($id === 0) {
            $this->tableGateway->insert($data);
            return;
        }

        try {
            $this->getCustomer($id);
        } catch (RuntimeException $e) {
            throw new RuntimeException(sprintf(
                'Cannot update album with identifier %d; does not exist',
                $id
            ));
        }

        $this->tableGateway->update($data, ['id' => $id]);
    }

    public function deleteCustomer($id)
    {
        $this->tableGateway->delete(['id' => (int) $id]);
    }
}

?>